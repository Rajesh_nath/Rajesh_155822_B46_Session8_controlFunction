<?php
$exp=true;

$i = 1;
while($i<10){

    echo "I'm inside the while Loop.". $i;
    $i++;
}//end of while loop

do{
    echo "I'm inside the do while Loop. ";
    $i++;
}while($i<10);//end of do while Loop


for($i=1,$j=10,$k=20;$k>$i;$i++,$j+=2,$k--)
{
    if($j>$k) break;
    echo "  $i";
}


$ageArray = array("JorinarBap"=>52, "Abul"=>35, "Kuddus"=>40);
foreach($ageArray as $key=>$value)
{
    echo $key. "=>".$value."<br>";
}